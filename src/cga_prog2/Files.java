package cga_prog2;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

public class Files {
        
    String chain = "";
    String first_Word = "";
    String second_Word = "";
    String third_Word = "";
    String chain_auxiliar = "";
    int empty_Lines = 0;
    int lines =0;
    int comment =0;
    int methods =0;
    int result=0;
    int cont=0;
    int total_Lines=0;
    int total_methods=0;
    File[] open;
    //M
    public int Read () throws FileNotFoundException {
        JFileChooser selectFile = new JFileChooser();
        selectFile.setMultiSelectionEnabled(true);
        
        FileNameExtensionFilter filter = new FileNameExtensionFilter("*.java", "java");
        selectFile.showOpenDialog(selectFile);
        open = selectFile.getSelectedFiles();
        return 0;
    }
    //M
    public int Count() throws IOException {
        try {
            for(int i = 0; i < open.length; i++) {
                if(open!=null) {
                    lines=0;
                    methods=0;
                    empty_Lines=0;
                    result=0;
                    
                    FileReader fileR = new FileReader(open[i]);
                    BufferedReader bufferedR = new BufferedReader(fileR);
                    
                    while ((chain_auxiliar = bufferedR.readLine())!=null) {
                   chain_auxiliar = chain_auxiliar.trim();
                       
                    
                    if(chain_auxiliar.equals("//M")) {
                        
                        lines++;
                        methods++;
                        total_Lines++;
                        total_methods++;
                    }
                    else if(!chain_auxiliar.equals("}")
                            && !chain_auxiliar.equals("")) {
                        lines++;
                        total_Lines++;
                    }
                    }
                    bufferedR.close();
                    
                    result = lines + empty_Lines;
                    
                    System.out.println("Name:----  " +open[i] + "\nLines----  " + result + "\nMethods----- " + methods);
                
                }
        
                }
        }
        catch (IOException e) {
            
        }
         
        
        
        System.out.println("Total of Lines:" + total_Lines);
        System.out.println("Total of Methods:" + total_methods);
        return 0;
    }
}